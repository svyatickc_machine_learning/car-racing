let a = true;
const CoordinateSystem = {
  a(val) { a = val; },
  // https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
  // check if AB intersects CD
  ccw(A,B,C) {
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x);
  },

  intersects(A,B,C,D) {
    return this.ccw(A,C,D) != this.ccw(B,C,D) && this.ccw(A,B,C) != this.ccw(A,B,D);
  },

  // https://stackoverflow.com/a/20679579/10462071
  // detect calculate poit
  // coefs(A, B) {
  //   return [A.y - B.y, B.x - A.x, (B.x * A.y) - (A.x * B.y)];
  // },

  // intersection(AB, CD) {
  //   const E = AB[0] * CD[1] - AB[1] * CD[0];
  //   if (E != 0) {
  //     const Ex = AB[2] * CD[1] - AB[1] * CD[2];
  //     const Ey = AB[0] * CD[2] - AB[2] * CD[0];
  //     return ({x: Ex/ E, y: Ey / E});
  //   }
  // },

  // intersection point for AB CD(vectors)
  intersection(A, B, C, D) {
    xdiff = [A.x - B.x, C.x - D.x];
    ydiff = [A.y - B.y, C.y - D.y];
    det = (a, b) => a[0] * b[1] - a[1] * b[0];

    div = det(xdiff, ydiff);
    if (div !== 0) {
      d = [det([A.x, A.y], [B.x, B.y]), det([C.x, C.y], [D.x, D.y])];
      x = det(d, xdiff) / div;
      y = det(d, ydiff) / div;
      // check if point belongs to line https://stackoverflow.com/a/907491/10462071
      // (((x - A.x) * (B.x - A.x)) + (y - A.y) * (B.y - A.y) >= 0 &&
      if (CoordinateSystem.isBetween(A, B, {x, y}) &&
          CoordinateSystem.isBetween(C, D, {x, y})) {
        return {x, y};
      }
    }
  },

  // https://stackoverflow.com/a/328122/10462071
  // check if C is betweem AB
  isBetween(A, B ,C) {
    const dotproduct = (x - A.x) * (B.x - A.x) + (y - A.y)*(B.y - A.y);
    if (dotproduct < 0) {
      return false;
    }
    const squaredlengthba = (B.x - A.x)*(B.x - A.x) + (B.y - A.y)*(B.y - A.y);
    if (dotproduct > squaredlengthba) {
      return false;
    }
    return true;
  },

  // https://stackoverflow.com/a/5228392/10462071
  distance(A, B) {
    return Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
  },

  // getting coordinates moved with distance on angle
  // https://math.stackexchange.com/questions/143932/calculate-point-given-x-y-angle-and-distance
  move({rad, x, y, distance}) {
    if (x !== undefined && y !== undefined) {
      throw 'Given 2 coordinates, expected 1';
    }
    if (x !== undefined) { return x + distance * Math.cos(rad); }
    if (y !== undefined) { return y + distance * Math.sin(rad); }
  },
};
