class Car {
  constructor(ctx) {
    this.ctx = ctx;
    this.x = 60; // x coordinate
    this.y = 60; // y coordinate
    this.d = 270; // degree
    this.corners = [];
    this.sight = [
      [0, 240], [15, 240], [45, 240],
      [90, 240], [135, 240], [180, 240],
      [225, 240], [270, 240], [315, 240],
      [345, 240],
    ]; // [angle, distance]
    this.sightLimitPoints = [];
    this.sightCaptures = []; // [{x: 1, y: 2}]; captures with the track
    this.allCaptures = [];
    this.distances = [];
    this.calculateCoordinates();
  }

  calculateCoordinates() {
    // car corners
    [30, 150, 210, 330].forEach((angle, i) => {
      const rad = Math.rad(this.d + angle);
      this.corners[i] = {
        x: CoordinateSystem.move({x: this.x, rad, distance: 15}),
        y: CoordinateSystem.move({y: this.y, rad, distance: 15}),
      };
    });
    this.sight.forEach(([angle, distance], i) => {
      const rad = Math.rad(this.d + angle);
      this.sightLimitPoints[i] = {
        x: CoordinateSystem.move({x: this.x, rad, distance}),
        y: CoordinateSystem.move({y: this.y, rad, distance}),
      };
    });
  }

  move(distance) {
    const rad = Math.rad(this.d);
    this.x = CoordinateSystem.move({x: this.x, distance, rad});
    const yWas = this.y;
    this.y = CoordinateSystem.move({y: this.y, distance, rad});
    if (!this.y) console.log(yWas);
  }

  intersectsWith(A, B) {
    let intersects = false;
    this.corners.map((corners, i) => {
      const ii = i === this.corners.length - 1 ? 0 : i + 1;
      const result = CoordinateSystem.intersects(
        {x: corners[0], y: corners[1]},
        {x: this.corners[ii][0], y: this.corners[ii][1]},
        A, B
      );
      if(result) { intersects = true; }
    });
    this.intersects = intersects;
    return intersects;
  }

  look(track) {
    // lines.forEach(())
    this.sightCaptures = [];
    this.allCaptures = [];
    this.distances = [];
    this.sightLimitPoints.forEach(limitPoint => {
      let intersection = null;
      let distance = null;
      track.forEach((trackPoint, i) => {
        const ii = i === track.length - 1 ? 0 : i + 1;
        const trackLineIntersection = CoordinateSystem.intersection(
          { x: this.x, y: this.y }, limitPoint, trackPoint, track[ii]
        );
        if (trackLineIntersection) {
          // console.count('check');
          const trackDistance = (CoordinateSystem.distance(
            trackLineIntersection, {x: this.x, y: this.y}
          )).toFixed(2);
          if (!intersection || (trackDistance < distance)) {
            if (intersection) { this.allCaptures.push(intersection); }
            distance = trackDistance;
            intersection = trackLineIntersection;
          } else {
            this.allCaptures.push(intersection);
          }
        }
      });
      if (intersection) {
        this.sightCaptures.push(intersection);
        this.distances.push(distance);
      } else {
        this.distances.push(0);
      }
    });
  }
}
