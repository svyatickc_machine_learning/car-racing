// TODO: add track editor
// canvas click coordinates:
// https://www.geeksforgeeks.org/how-to-get-the-coordinates-of-a-mouse-click-on-a-canvas-element/#:~:text=The%20position%20of%20x%2Dcoordinate,using%20the%20'left'%20property.
const canvas = document.getElementById('game');
const distance = document.getElementById('distance');
window.canvas = canvas;
let renderTrack = true;
let renderSight = true;

// [x1, y1, x2, y2]
const trackOuter = [];

const car = new Car();
const ctx = canvas.getContext('2d');

window.car = car;

function draw() {
  // clean
  ctx.beginPath();
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  ctx.fillStyle = 'white';
  ctx.stroke();
  ctx.closePath();

  // draw track
  if (renderTrack) {
    if(trackOuter.length) {
      ctx.beginPath();
      ctx.moveTo(trackOuter[0].x, trackOuter[0].y);
      trackOuter.slice(1).forEach(({x, y}) => ctx.lineTo(x, y));
      ctx.lineTo(trackOuter[0].x, trackOuter[0].y);
      ctx.stroke();
      ctx.closePath();
    }
  }

  // draw car
  // rendering polygons https://stackoverflow.com/questions/4839993/how-to-draw-polygons-on-an-html5-canvas
  ctx.beginPath();
  ctx.fillStyle = car.intersects ? 'red' : 'black';
  ctx.moveTo(car.corners[0].x, car.corners[0].y);
  car.corners.slice(1).forEach(({x, y}) => ctx.lineTo(x, y));
  ctx.fill();
  ctx.closePath();

  // draw car's center
  // ctx.beginPath();
  // ctx.fillStyle = 'black';
  // ctx.arc(car.x, car.y, 1, 0, 2 * Math.PI, false);
  // ctx.fill();
  // ctx.closePath();

  // draw sight lines
  if (renderSight) {
    car.sightLimitPoints.forEach(({x, y}) => {
      ctx.beginPath();
      ctx.fillStyle = 'black';
      ctx.moveTo(car.x, car.y);
      ctx.lineTo(x, y);
      ctx.stroke();
      ctx.closePath();
    });
  }

  // draw sight captures
  car.sightCaptures.forEach(({x, y}) => {
    ctx.beginPath();
    ctx.fillStyle = 'red';
    ctx.arc(x, y, 5, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.closePath();
  });

  car.allCaptures.forEach(({x, y}) => {
    ctx.beginPath();
    ctx.fillStyle = 'blue';
    ctx.arc(x, y, 3, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.closePath();
  });

  distance.innerText = car.distances.toString();
}

draw();

let forward = false;
let backward = false;
let left = false;
let right = false;

document.addEventListener('keydown', (e) => {
  if ([37, 38 ,39 ,40].includes(e.keyCode)) {
    e.preventDefault();
    if (e.keyCode === 38) {
      forward = true;
    } else if (e.keyCode === 40) {
      backward = true;
    } else if (e.keyCode === 37) {
      left = true;
    } else if (e.keyCode === 39) {
      right = true;
    }
  }
});

document.addEventListener('keyup', (e) => {
  if (e.keyCode === 38) {
    forward = false;
  } else if (e.keyCode === 40) {
    backward = false;
  } else if (e.keyCode === 37) {
    left = false;
  } else if (e.keyCode === 39) {
    right = false;
  }
});

canvas.addEventListener('click', (e) => {
  const rect = canvas.getBoundingClientRect();
  const x = e.clientX - rect.left;
  const y = e.clientY - rect.top;
  trackOuter.push({x, y});
});

function move() {
  if (left) { car.d -= 1; }
  if (right) { car.d += 1; }
  if (forward) { car.move(1); }
  if (backward) { car.move(-1); }
  car.calculateCoordinates();
  trackOuter.forEach((trackPoint, i) => {
    const ii = i === trackOuter.length - 1 ? 0 : i + 1;
    car.intersectsWith(trackPoint, trackOuter[ii]);
  });
  car.look(trackOuter);
  draw();
}

setInterval(move, 1);
// setInterval(draw, 30);
